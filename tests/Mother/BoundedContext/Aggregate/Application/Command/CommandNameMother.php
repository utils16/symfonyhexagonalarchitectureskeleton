<?php

declare(strict_types=1);

namespace PROJECT\Tests\Mother\BoundedContext\Aggregate\Application\Command;

use PROJECT\BoundedContext\Aggregate\Application\Command\CommandName;

class CommandNameMother
{
    /**
     * @return CommandName
     */
    public static function create(): CommandName
    {
        return new CommandName();
    }
}