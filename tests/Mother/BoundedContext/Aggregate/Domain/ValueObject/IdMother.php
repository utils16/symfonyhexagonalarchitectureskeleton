<?php

declare(strict_types=1);

namespace PROJECT\Tests\Mother\BoundedContext\Aggregate\Domain\ValueObject;

use Faker\Factory;
use Exception;
use PROJECT\BoundedContext\Aggregate\Domain\ValueObject\Id;

final class IdMother
{
    /**
     * @param string $id
     * @return Id
     * @throws Exception
     */
    public static function create(string $id): Id
    {
        return Id::fromString($id);
    }

    /**
     * @return Id
     * @throws Exception
     */
    public static function random(): Id
    {
        $faker = Factory::create();
        return self::create((string) $faker->randomNumber());
    }
}
