<?php

declare(strict_types=1);

namespace PROJECT\Tests\Mother\BoundedContext\Aggregate\Domain\Entity;

use Exception;
use PROJECT\BoundedContext\Aggregate\Domain\Entity\Entity;

class EntityMother
{
    /**
     * @return Entity
     */
    public static function create(): Entity
    {
        return Entity::instantiate();
    }

    /**
     * @return Entity
     * @throws Exception
     */
    public static function random(): Entity
    {
        return self::create();
    }
}
