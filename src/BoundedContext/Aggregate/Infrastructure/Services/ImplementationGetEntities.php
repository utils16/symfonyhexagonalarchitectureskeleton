<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Aggregate\Infrastructure\Services;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Exception;
use PROJECT\BoundedContext\Aggregate\Application\DataTransformer\EntityDataTransformer;
use PROJECT\BoundedContext\Aggregate\Domain\Entity\Entity;
use PROJECT\BoundedContext\Aggregate\Domain\Services\GetEntities;
use PROJECT\BoundedContext\Aggregate\Infrastructure\Exception\ExceptionName;
use PROJECT\BoundedContext\Shared\Domain\Services\Cache\CacheServiceReader;
use PROJECT\BoundedContext\Shared\Domain\Services\Cache\CacheServiceWriter;
use PROJECT\BoundedContext\Shared\Infrastructure\Services\Http\HttpClient;
use PROJECT\BoundedContext\Shared\Infrastructure\Services\Http\HttpClientResponse;

class ImplementationGetEntities implements GetEntities
{
    private EntityDataTransformer $dataTransformer;
    private HttpClient $httpClient;
    private string $urlService;

    private CacheServiceReader $cacheServiceReader;
    private CacheServiceWriter $cacheServiceWriter;

    /**
     * ImplementationGetEntities constructor.
     * @param EntityDataTransformer $dataTransformer
     * @param HttpClient $httpClient
     * @param string $urlService
     * @param CacheServiceReader $cacheServiceReader
     * @param CacheServiceWriter $cacheServiceWriter
     */
    public function __construct(
        EntityDataTransformer $dataTransformer,
        HttpClient $httpClient,
        string $urlService,
        CacheServiceReader $cacheServiceReader,
        CacheServiceWriter $cacheServiceWriter
    ) {
        $this->urlService = $urlService;
        $this->dataTransformer = $dataTransformer;
        $this->httpClient = $httpClient;
        $this->cacheServiceReader = $cacheServiceReader;
        $this->cacheServiceWriter = $cacheServiceWriter;
    }

    /**
     * @return Entity[]
     * @throws Exception|GuzzleException
     */
    public function execute(): array
    {
        $entities = [];

        try {
            if (!empty($this->cacheServiceReader->execute('dataListService'))) {
                return $this->cacheServiceReader->execute('dataListService');
            }

            $serviceResponse = $this->get($this->urlService);
            $response = json_decode($serviceResponse->getBody(), true);

            if (empty($response)) {
                ExceptionName::throw();
            }

            foreach ($response as $entity) {
                $entities[] = $this->dataTransformer->transform($entity);
            }

            $this->cacheServiceWriter->execute('dataListService', $entities);
        } catch (ClientException $exception) {
            $this->manageException($exception);
        }
        return $entities;
    }

    /**
     * @param string $endPoint
     * @param array $parameters
     * @return HttpClientResponse
     * @throws GuzzleException
     */
    public function get(string $endPoint, array $parameters = []): HttpClientResponse
    {
        $parametersString = !empty($parameters) ? '?' . http_build_query($parameters) : '';
        $uri = $endPoint . $parametersString;

        return $this->httpClient->request('GET', $uri);
    }

    /**
     * @param Exception $exception
     * @throws Exception
     */
    protected function manageException(Exception $exception): void
    {
        $code = $exception->getCode();

        if ($code == 404) {
            ExceptionName::throw();
        }

        throw $exception;
    }
}