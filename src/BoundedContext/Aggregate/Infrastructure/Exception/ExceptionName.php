<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Aggregate\Infrastructure\Exception;

use Exception;
use PROJECT\BoundedContext\Shared\Domain\Exception\Http\ItemNotFound;

final class ExceptionName extends Exception implements ItemNotFound
{
    /**
     * @throws static
     */
    public static function throw(): void
    {
        throw new static('Message for item not found', 404);
    }
}