<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Aggregate\Domain\Entity;

class Entity
{

    /**
     * Entity constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return static
     */
    public static function instantiate(): self
    {
        return new static();
    }

}
