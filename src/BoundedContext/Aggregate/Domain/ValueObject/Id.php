<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Aggregate\Domain\ValueObject;

use PROJECT\Aggregate\Shared\Domain\ValueObject\StringValueObject;

final class Id extends StringValueObject
{

}