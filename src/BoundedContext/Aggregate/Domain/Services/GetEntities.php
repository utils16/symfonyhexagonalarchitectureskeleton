<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Aggregate\Domain\Services;

use PROJECT\BoundedContext\Aggregate\Domain\Entity\Entity;

interface GetEntities
{
    /**
     * @return Entity[]
     */
    public function execute(): array;
}