<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Aggregate\Application\DataTransformer;

use PROJECT\BoundedContext\Aggregate\Domain\Entity\Entity;
use PROJECT\BoundedContext\Shared\Application\DataTransformer\DataTransformer;

class EntityDataTransformer implements DataTransformer
{
    /**
     * @param mixed $data
     * @return Entity
     */
    public function transform($data): Entity
    {
        return Entity::instantiate();
    }
}