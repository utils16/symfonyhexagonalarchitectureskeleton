<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Aggregate\Application\Command\Handler;

use PROJECT\BoundedContext\Aggregate\Application\Command\CommandName;

class HandlerNameHandler
{

    /**
     * HandlerNameHandler constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param CommandName $command
     * @return array
     */
    public function handle(CommandName $command): array
    {
        return [];
    }
}