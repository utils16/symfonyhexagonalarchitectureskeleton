<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Shared\Infrastructure\Services\Cache;

use PROJECT\BoundedContext\Shared\Domain\Services\Cache\CacheServiceReader;
use Psr\Cache\InvalidArgumentException;

class SymfonyCacheServiceReader extends SymfonyCacheService implements CacheServiceReader
{
    /**
     * SymfonyCacheServiceReader constructor.
     * @param string $namespace
     */
    public function __construct(string $namespace)
    {
        parent::__construct($namespace);
    }

    /**
     * @param string $key
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function execute(string $key)
    {
        return $this->cache->getItem($key)->get();
    }

}