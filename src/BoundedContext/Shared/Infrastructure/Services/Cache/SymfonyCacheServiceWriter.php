<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Shared\Infrastructure\Services\Cache;

use PROJECT\BoundedContext\Shared\Domain\Services\Cache\CacheServiceWriter;
use Psr\Cache\InvalidArgumentException;

class SymfonyCacheServiceWriter extends SymfonyCacheService implements CacheServiceWriter
{
    private int $ttl;

    /**
     * SymfonyCacheServiceWriter constructor.
     * @param string $namespace
     * @param int $ttl
     */
    public function __construct(string $namespace, int $ttl = 0)
    {
        $this->ttl = $ttl;
        parent::__construct($namespace, $ttl);
    }

    /**
     * @param string $key
     * @param $object
     * @throws InvalidArgumentException
     */
    public function execute(string $key, $object)
    {
        if($this->ttl != 0) {
            $cacheItem = $this->cache->getItem($key);
            $cacheItem->set($object);

            $cacheItem->expiresAfter($this->ttl);

            $this->cache->save($cacheItem);
        }
    }

}