<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Shared\Application\DataTransformer;

interface DataTransformer
{
    /**
     * @param mixed $data
     * @return mixed
     */
    public function transform($data);
}