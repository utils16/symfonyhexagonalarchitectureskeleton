<?php

namespace PROJECT\BoundedContext\Shared\Domain\Services\Cache;

interface CacheServiceReader
{
    public function execute(string $key);
}