<?php

namespace PROJECT\BoundedContext\Shared\Domain\Services\Cache;

interface CacheServiceWriter
{
    public function execute(string $key, $object);
}