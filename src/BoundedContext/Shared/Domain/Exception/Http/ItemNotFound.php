<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Shared\Domain\Exception\Http;

interface ItemNotFound
{
    public static function throw(): void;
}