<?php

declare(strict_types=1);

namespace PROJECT\BoundedContext\Shared\Domain\Exception\Http;

interface ServerError
{
    public static function throw(): void;
}